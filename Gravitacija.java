import java.util.*;


class Gravitacija {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double v = sc.nextDouble();
        izpis(v, gp(v));
        
    }
    
    public static void izpis(double v, double g) {
        System.out.println("visina: "+v+" m");
        System.out.println("gravitacijski pospesek: "+gp(v)+" m/(s^2)");
    }
    
    public static double gp(double v){
        double C=6.674*Math.pow(10,-11);
        double M=5.972*Math.pow(10,24);
        double r=6.371*Math.pow(10,6);
        return (C*M)/(Math.pow(r+v,2));
    }
}
